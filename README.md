This is a simple Tic Tac Toe game for android devices. Allows portrait and landscape orientations.

What you need to run the project: 

- [Android studio 13](https://developer.android.com/studio?gclid=CjwKCAjw2P-KBhByEiwADBYWCnKCXpyLO9_4GeKSvOnBK9H1z65M1H2PiJQYMU3eiy8sneytbOH-RRoCHqcQAvD_BwE&gclsrc=aw.ds)

- [Java 17](https://www.oracle.com/java/technologies/downloads/)

- [Gradle 3.2](https://gradle.org/install/)

- Android device with Android OS Jelly Bean or higher.
